package tshabalala.bongani.cobiassignment.Helper;

public class Constants {

    public final static String URL = "http://codetest.cobi.co.za/androids.json"; //JSON file from the URL
    public final static String IMAGE_PATH = "http://codetest.cobi.co.za/"; // Url path of the image
    public final static String ANDROID_VERSION = "Android Version"; // Variable passed by an Intent
    public final static String JSONARRAY_VARIABLE = "versions"; // Variable passed into the json array
}
