package tshabalala.bongani.cobiassignment.Controller;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import tshabalala.bongani.cobiassignment.Helper.Constants;
import tshabalala.bongani.cobiassignment.Model.AndroidVersion;
import tshabalala.bongani.cobiassignment.R;

public class VersionDetailActivity extends AppCompatActivity {

    //Variable used
    private TextView txtAndroidName;
    private TextView txtAndroidVersion;
    private TextView txtAndroidReleaseDate;
    private TextView txtAndroidApi;
    private ImageView imageView;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_version_details);

        //Initialization of UI elements
        initUI();

        //Getting the AndroidVersion passed by an Intent
        AndroidVersion androidVersion = (AndroidVersion) getIntent().getSerializableExtra(Constants.ANDROID_VERSION);
        if(androidVersion != null){

            String image = Constants.IMAGE_PATH+androidVersion.getImage();
            /**Getting the image from path
             * Substring the image path to check the image path is not equal to null
             * Displaying the default image if path is null
             * Else actual image
             */
            if((image.substring(Constants.IMAGE_PATH.length(), image.length())).equals("null")){

                Picasso.with(VersionDetailActivity.this).load(R.drawable.no_image).into(imageView);
            }else{
                Picasso.with(VersionDetailActivity.this).load(image).into(imageView);
            }
            //Displaying version details
            txtAndroidName.setText(getString(R.string.android_name)  + androidVersion.getName());
            txtAndroidVersion.setText(getString(R.string.android_version) +androidVersion.getVersion());
            txtAndroidReleaseDate.setText(getString(R.string.android_release_date) +androidVersion.getRelease());
            txtAndroidApi.setText(getString(R.string.android_api) +androidVersion.getApi());
        }
    }

    /**Initialization method of UI elements*/
    public void initUI()
    {
        imageView = findViewById(R.id.imageView);
        txtAndroidName = findViewById(R.id.textAndroidName);
        txtAndroidVersion = findViewById(R.id.textAndroidVersion);
        txtAndroidReleaseDate = findViewById(R.id.textAndroidDate);
        txtAndroidApi = findViewById(R.id.textAndroidApi);

    }
}
