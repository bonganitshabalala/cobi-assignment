package tshabalala.bongani.cobiassignment.Controller;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import tshabalala.bongani.cobiassignment.Adapter.AndroidVersionCustomAdapter;
import tshabalala.bongani.cobiassignment.Helper.Constants;
import tshabalala.bongani.cobiassignment.Model.AndroidVersion;
import tshabalala.bongani.cobiassignment.R;

public class MainActivity extends AppCompatActivity {

    //Variables used
    private List<AndroidVersion> versionList = new ArrayList<>();
    private static final String TAG = "MainActivity";
    private ListView listView;
    private AndroidVersionCustomAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.androidVersionList);

        /**Checking if internet connection is available
         * If yes, calls a method(connectToApi) to get the JSON file
         * Else show message to user
         */
        if (!isOnline(MainActivity.this)) {
            Toast.makeText(MainActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
        }else {
            connectToApi();
        }

    }

    // Method to get a JSON file the URL
    private void connectToApi() {

        //Starting progress dialog, shows user some processing is happening
        final ProgressDialog pd = new ProgressDialog(MainActivity.this);
        pd.setMessage("Loading data...");
        pd.show();

        /**
         * Using a volley library to connect to the URL, by method GET
         *
          */
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, Constants.URL, null, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                    // Getting JSON Array node
                try {
                    JSONArray jsonArray = response.getJSONArray(Constants.JSONARRAY_VARIABLE);
                    // looping through All Version
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        AndroidVersion androidVersion = gson.fromJson(obj.toString(), AndroidVersion.class);
                        versionList.add(androidVersion);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //Passing version list to a method
                displayToList(versionList);
                //dismiss the progress dialog
                pd.dismiss();
            }
        },new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                System.out.println("" + volleyError);
                //Displaying an error to a user if there is network errors
                Toast.makeText(MainActivity.this, volleyError.getMessage(), Toast.LENGTH_LONG).show();

                pd.dismiss();
            }
        });

        RequestQueue rQueue = Volley.newRequestQueue(MainActivity.this);
        rQueue.add(request);
    }

    //Method containing a list of android versions
    private void displayToList(List<AndroidVersion> versionList) {
        //Interface implementation onClick on a card view
        AndroidVersionCustomAdapter.OnItemClickListener versionClickListener = new AndroidVersionCustomAdapter.OnItemClickListener() {
            @Override
            public boolean onItemClick(AndroidVersion androidVersion) {
                //Intent used to go to another activity
                Intent intent = new Intent(MainActivity.this,VersionDetailActivity.class);
                intent.putExtra(Constants.ANDROID_VERSION,androidVersion);
                startActivity(intent);
                return true;
            }

        };

        //Checking if list is empty
        if(versionList.size() > 0){

            //Inflating the custom adapter with data
            adapter = new AndroidVersionCustomAdapter(this, android.R.layout.simple_list_item_1, versionList, versionClickListener);
            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

        }
    }

    //Method used to check if user has internet connection
    private boolean isOnline(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Do something that differs the Activity's menu here
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_reload:
                //Calling the method(connectToApi) if there is a problem with the internet
                if (!isOnline(MainActivity.this)) {
                    Toast.makeText(MainActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
                }else {
                    //Clear list if populated
                    if(versionList.size() > 0){
                        adapter.clear();
                    }
                    connectToApi();
                }
                break;
            default:
                break;
        }

        return false;
    }

}
