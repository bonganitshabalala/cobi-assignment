package tshabalala.bongani.cobiassignment.Model;

import java.io.Serializable;

public class AndroidVersion implements Serializable{

    private String name;
    private String version;
    private String released;
    private String api;
    private String image;

    public AndroidVersion() {
    }

    public AndroidVersion(String name, String version, String released, String api, String image) {
        this.name = name;
        this.version = version;
        this.released = released;
        this.api = api;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getRelease() {
        return released;
    }

    public void setRelease(String release) {
        this.released = release;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
