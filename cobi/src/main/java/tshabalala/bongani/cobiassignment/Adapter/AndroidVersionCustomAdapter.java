package tshabalala.bongani.cobiassignment.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import tshabalala.bongani.cobiassignment.Helper.Constants;
import tshabalala.bongani.cobiassignment.Model.AndroidVersion;
import tshabalala.bongani.cobiassignment.R;

public class AndroidVersionCustomAdapter extends ArrayAdapter {

    private Activity mContext = null;
    private LayoutInflater mInflater = null;
    private List<AndroidVersion> androidVersionsList;
    private static OnItemClickListener itemClickListener;


    //UI elements for xml file
    private static class ViewHolder {
        private ImageView mIcon = null;
        private TextView mText = null;
        private CardView cardView = null;
    }

    //Constructor
    public AndroidVersionCustomAdapter(Activity context, int textViewResourceId, List<AndroidVersion> androidVersionsList, OnItemClickListener versionClickListener){
        super(context, textViewResourceId, androidVersionsList);
        this.mContext = context;
        this.androidVersionsList = androidVersionsList;
        itemClickListener = versionClickListener;
        mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View getView(int position, View contentView, final ViewGroup parent){
        final ViewHolder vh;
        final AndroidVersion androidVersion = androidVersionsList.get(position);
        if(contentView == null){
            vh = new ViewHolder();
            contentView = mInflater.inflate(R.layout.adapter_detail_structure, parent, false);
            vh.mIcon = contentView.findViewById(R.id.imageView);
            vh.mText = contentView.findViewById(R.id.textView);
            vh.cardView =  contentView.findViewById(R.id.card_view);
            contentView.setTag(vh);
        } else {
            vh = (ViewHolder) contentView.getTag();
        }

        /**Getting the image from path
         * Substring the image path to check the image path is not equal to null
         * Displaying the default image if path is null
         * Else actual image
         */
        String image = Constants.IMAGE_PATH+androidVersion.getImage();
        if((image.substring(Constants.IMAGE_PATH.length(), image.length())).equals("null")){

            Picasso.with(mContext).load(R.drawable.no_image).into(vh.mIcon);
        }else{
            Picasso.with(mContext).load(image).into(vh.mIcon);
        }

        vh.mText.setText(androidVersion.getName());
        setRandomTextColor(vh.mText);

        vh.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (itemClickListener != null) {
                    itemClickListener.onItemClick(androidVersion);
                }

            }
        });

        return (contentView);
    }

    //OnClick interface for Card view
    public interface OnItemClickListener{
        boolean onItemClick(AndroidVersion androidVersion);

    }

    //Setting a random color to a textview(android name)
    public void setRandomTextColor(TextView textColor){
        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        textColor.setTextColor(color);
    }
}